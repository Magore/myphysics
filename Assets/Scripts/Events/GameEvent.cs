﻿using System.Collections.Generic;
using UnityEngine;

namespace Events
{
    [CreateAssetMenu(fileName = "NewEvent", menuName = "Event", order = 2)]
    public class GameEvent : ScriptableObject
    {
        private List<GameEventListener> m_Listeners = new List<GameEventListener>();

        public void Invoke(MonoBehaviour mono)
        {
            for (int i = m_Listeners.Count - 1; i >= 0; i--)
            {
                m_Listeners[i].EventRaised(mono);
            }
        }

        public void RegisterListener(GameEventListener listener)
        {
            m_Listeners.Add(listener);
        }

        public void UnregisterListener(GameEventListener listener)
        {
            m_Listeners.Remove(listener);
        }
    }
}