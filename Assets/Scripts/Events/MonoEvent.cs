﻿using UnityEngine;
using UnityEngine.Events;

namespace Events
{
    [System.Serializable]
    public class MonoEvent : UnityEvent<MonoBehaviour>
    {

    }
}