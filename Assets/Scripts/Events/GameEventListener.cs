﻿using System.Collections.Generic;
using UnityEngine;

namespace Events
{
    public class GameEventListener : MonoBehaviour
    {
        public GameEvent m_Events;
        public MonoEvent m_Response;


        private void OnEnable()
        {
            m_Events.RegisterListener(this);

        }

        private void OnDisable()
        {
            m_Events.UnregisterListener(this);
        }

        public void EventRaised(MonoBehaviour author)
        {
            m_Response.Invoke(author);
        }
    }
}