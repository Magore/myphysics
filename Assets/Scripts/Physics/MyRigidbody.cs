﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace Physics
{
    public class MyRigidbody : MonoBehaviour
    {
        #region Members

        [HideInInspector]
        public MyCollider thisCollider;
        public Vector3 velocity;
        public float mass = 1;
        public bool useGravity = true;
        public bool isKinematic = false;

        protected float friction = 0f;

        protected List<MyContact> connectedCollisions = new List<MyContact>();
        protected List<MyCollider> connectedTriggers= new List<MyCollider>();

        #endregion

        #region UnityBehavior

        private void Start()
        {
            CalculateFriction();
        }

        protected void FixedUpdate()
        {
            Gravity();
            CheckCollision();
            Move();
        }

        protected void OnValidate()
        {
            thisCollider = GetComponent<MyCollider>();
        }

        #endregion

        #region Protected_Methods

        protected void CheckCollision()
        {
            if (!thisCollider) return;  //thisCollider not attached
            foreach (var otherCollider in MyPhysics.colliders)
            {
                if (!otherCollider.gameObject.activeInHierarchy || !otherCollider.gameObject.activeInHierarchy) continue;
                //check with itself
                if (otherCollider == thisCollider || otherCollider.attachedRigidbody == this) continue;
                //check collision mask
                if (thisCollider.collisionLayer != (thisCollider.collisionLayer | 1 << otherCollider.gameObject.layer)) continue;

                bool isTrigger = otherCollider.isTrigger;
                //does colliders were in touch
                int index = isTrigger ? connectedTriggers.FindIndex((obj) => obj == otherCollider) : connectedCollisions.FindIndex((obj) => obj.otherCollider == otherCollider);

                bool isOverlap = thisCollider.Overlap(otherCollider); //does colliders in touch now

                if (!isOverlap)
                {
                    //was overlap
                    if (index != -1)
                    {
                        if (isTrigger)
                        {
                            connectedTriggers[index].OnTriggerExit?.Invoke(thisCollider);   //event TriggerExit
                            thisCollider.OnTriggerExit?.Invoke(connectedTriggers[index]);
                            connectedTriggers.RemoveAt(index);
                        }
                        else
                        {
                            //event CollisionExit
                            connectedCollisions[index].otherCollider.OnCollisionExit?.Invoke(thisCollider);
                            thisCollider.OnCollisionExit?.Invoke(connectedCollisions[index].otherCollider);
                            connectedCollisions.RemoveAt(index);

                            CalculateFriction();
                        }
                    }
                    continue;
                }
                ; //if not, check next thisCollider

                //else resolve collision or trigger

                if (isTrigger)
                {
                    ResolveTrigger(thisCollider, otherCollider, index);
                }
                else
                {
                    ResolveCollision(thisCollider, otherCollider, index);
                }
            }
        }

        protected void ResolveTrigger(MyCollider a, MyCollider b, int index)
        {
            if (index == -1)
            {
                a.OnTriggerEnter?.Invoke(b);
                b.OnTriggerEnter?.Invoke(a);
                CalculateFriction();
            }
            else
            {
                a.OnTriggerStay?.Invoke(b);
                b.OnTriggerStay?.Invoke(a);
            }
        }

        protected void ResolveCollision(MyCollider a, MyCollider b, int index)
        {
            MyContact contactAB;

            //does colliders were in touch
            if (index == -1)
            {
                contactAB = new MyContact(a, b);
                connectedCollisions.Add(contactAB);
                a.OnCollisionEnter?.Invoke(b);
                b.OnCollisionEnter?.Invoke(a);
                CalculateFriction();
            }
            else
            {
                contactAB = connectedCollisions[index];
                a.OnCollisionStay?.Invoke(b);
                b.OnCollisionStay?.Invoke(a);
            }

            var N = contactAB.normal;
            if (Vector3.Dot(velocity, N) > 0)
            {
                if (b.attachedRigidbody)
                {
                    b.attachedRigidbody.AddForce(N * Vector3.Dot(N, velocity) * mass);
                }
                var bounce = (index == -1) ?  + a.material.bounciness : 0 ;
                velocity -= (1 + bounce) * N * Vector3.Dot(N, velocity);
            }
        }


        protected void CalculateFriction()
        {
            if (connectedCollisions.Count == 0)
            {
                friction = 0;
                return;
            }

            float sum = 0;

            for (int i = 0; i < connectedCollisions.Count; i++)
            {

                sum += connectedCollisions[i].otherCollider.material.friction;
            }
        
            sum += thisCollider.material.friction;
            friction = sum / (connectedCollisions.Count + 1);   //calculate friction as average of all connected colliders
        }

        protected void Move()
        {
            if (isKinematic) return;
            //subtract friction from velocity

            Vector3 newVelocity = velocity.normalized * friction;
            if ((Mathf.Abs(newVelocity.x) > Mathf.Abs(velocity.x)))     //check that friction won't make object move another way
            {
                newVelocity.x = newVelocity.x = velocity.x;
            }
            if ((Mathf.Abs(newVelocity.z) > Mathf.Abs(velocity.z)))     //check that friction won't make object move another way
            {
                newVelocity.z = newVelocity.z = velocity.z;
            }

            velocity = new Vector3(velocity.x - newVelocity.x,
                velocity.y,
                velocity.z - newVelocity.z);
            transform.position += (velocity * Time.deltaTime);
        }

        protected void Gravity()
        {
            if (isKinematic) return;
            if (useGravity && !isKinematic)
            {
                velocity += MyPhysics.Gravity * Time.deltaTime; 
            }
        }

        #endregion

        #region Public_Methods

        public void AddForce(Vector3 force)
        {
            if (isKinematic) return;
            velocity += force / mass;
        }

        #endregion

        #region Coroutines



        #endregion


    }
}
