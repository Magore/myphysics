﻿using System;
using UnityEngine;

namespace Physics
{
    public abstract class MyCollider : MonoBehaviour
    {
        #region Members

        [HideInInspector]
        public new Transform transform;

        [HideInInspector]
        public MyRigidbody attachedRigidbody;

        public bool isTrigger = false;
        public MyPhysicMaterial material;
        public LayerMask collisionLayer = ~0;


        [Header("Events")] public Action<MyCollider> OnTriggerEnter;
        public Action<MyCollider> OnTriggerExit;
        public Action<MyCollider> OnTriggerStay;

        public Action<MyCollider> OnCollisionEnter;
        public Action<MyCollider> OnCollisionExit;
        public Action<MyCollider> OnCollisionStay;


        #endregion

        public abstract bool Overlap(MyCollider other);

        #region UnityBehavior

        protected void Awake()
        {
            if (material == null)
            {
                material = Resources.Load<MyPhysicMaterial>("PhysicMaterials/StandardMaterial");
            }
            MyPhysics.colliders.Add(this);
        }

        private void OnDestroy()
        {
            MyPhysics.colliders.Remove(this);
        }

        protected void OnValidate()
        {
            transform = GetComponent<Transform>();
            attachedRigidbody = GetComponent<MyRigidbody>();
        }



        #endregion

    }
}
