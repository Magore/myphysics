﻿using System;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

namespace Physics
{
    public class MyBoxCollider : MyCollider
    {
        public Vector3 offset = Vector3.zero;
        public Vector3 size = Vector3.one;


        public Vector3 Center => transform.position + offset;
        public Vector3 HalfSize => size / 2;

        public override bool Overlap(MyCollider other)
        {
            if (other is MyBoxCollider otherBox)
            {
                if (Mathf.Abs(Center.x - otherBox.Center.x) > HalfSize.x + otherBox.HalfSize.x) return false;
                if (Mathf.Abs(Center.y - otherBox.Center.y) > HalfSize.y + otherBox.HalfSize.y) return false;
                if (Mathf.Abs(Center.z - otherBox.Center.z) > HalfSize.z + otherBox.HalfSize.z) return false;
                return true;
            }

            throw new Exception("Unknown collision check");
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireCube(Center, size);
        }
    }
}


