﻿using System.Collections.Generic;
using UnityEngine;

namespace Physics
{
    public class MyPhysics : MonoBehaviour
    {
        public static Vector3 Gravity = new Vector3(0, -9.81f, 0);

        public static List<MyCollider> colliders= new List<MyCollider>();

        public static MyCollider[] OverlapBox(Vector3 position, Vector3 size, int layerMask = ~0)
        {
            List<MyCollider> overlapedColliders = new List<MyCollider>();
            var halfSize = size / 2;

            LayerMask mask = layerMask;

            foreach (var col in overlapedColliders)
            {
                if(mask != (mask | 1 << col.gameObject.layer)) continue;
                if (col is MyBoxCollider boxCollider)
                {
                    if (Mathf.Abs(position.x - boxCollider.Center.x) > halfSize.x + boxCollider.HalfSize.x) continue;
                    if (Mathf.Abs(position.y - boxCollider.Center.y) > halfSize.y + boxCollider.HalfSize.y) continue;
                    if (Mathf.Abs(position.z - boxCollider.Center.z) > halfSize.z + boxCollider.HalfSize.z) continue;

                    overlapedColliders.Add(boxCollider);
                }
            }

            return overlapedColliders.ToArray();
        }
    }
}
