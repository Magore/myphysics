﻿using Vector3 = UnityEngine.Vector3;

namespace Physics
{
    public class MyContact
    {
        public MyCollider thisCollider;
        public MyCollider otherCollider;

        public Vector3 normal;
        //public Vector3 point;
        public float penetration;

        //face normals
        Vector3[] facesAABB =
        {
            new Vector3(-1,  0,  0), // 'left' face normal (-x direction)
            new Vector3( 1,  0,  0), // 'right' face normal (+x direction)
            new Vector3( 0, -1,  0), // 'bottom' face normal (-y direction)
            new Vector3( 0,  1,  0), // 'top' face normal (+y direction)
            new Vector3( 0,  0, -1), // 'far' face normal (-z direction)
            new Vector3( 0,  0,  1) // 'near' face normal (+x direction)
        };

        public MyContact()
        {

        }

        public MyContact(MyCollider its, MyCollider other)
        {
            thisCollider = its;
            otherCollider = other;
            if (its is MyBoxCollider a && other is MyBoxCollider b)
            {
                AABBvsAABB(a, b);
            }
        
        }

        protected void AABBvsAABB(MyBoxCollider a, MyBoxCollider b)
        {
        
            normal = Vector3.zero;
            penetration = 0f;


            float[] distance =
            {
                ((b.Center.x + b.HalfSize.x) - (a.Center.x - a.HalfSize.x)),    // distance of box 'b' to face on 'left' side of 'a'
                ((a.Center.x + a.HalfSize.x) - (b.Center.x - b.HalfSize.x)),    // distance of box 'b' to face on 'right' side of 'a'
                ((b.Center.y + b.HalfSize.y) - (a.Center.y - a.HalfSize.y)),    // distance of box 'b' to face on 'bottom' side of 'a'
                ((a.Center.y + a.HalfSize.y) - (b.Center.y - b.HalfSize.y)),    // distance of box 'b' to face on 'top' side of 'a'
                ((b.Center.z + b.HalfSize.z) - (a.Center.z - a.HalfSize.z)),    // distance of box 'b' to face on 'far' side of 'a'
                ((a.Center.z + a.HalfSize.z) - (b.Center.z - b.HalfSize.z))     // distance of box 'b' to face on 'near' side of 'a'

            };

            for (int i = 0; i < facesAABB.Length; i++)
            {
                if (i == 0 || distance[i] < 0.1f)
                {
                    normal = facesAABB[i];
                    penetration = distance[i];
                }
            }
        }

    }
}
