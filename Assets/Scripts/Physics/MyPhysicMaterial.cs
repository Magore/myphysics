﻿using UnityEngine;

namespace Physics
{
    [CreateAssetMenu(fileName = "PhysicMaterial", menuName = "MyPhysics/PhysicMaterial", order = 1)]
    public class MyPhysicMaterial : ScriptableObject
    {
        public float friction = 0.6f;
        [Range(0f, 1f)] public float bounciness;
    }
}
