﻿using System.Collections.Generic;
using UnityEngine;

namespace Core
{
    public class PoolingGameObject : MonoBehaviour
    {
        #region Members

        [SerializeField]
        protected GameObject m_Prefab;
        [SerializeField]
        protected int m_PooledAmount = 20;
        [SerializeField]
        protected bool m_WillGrow = true;
        [SerializeField]
        public List<GameObject> m_PooledObjects;

        #endregion

        #region Unity_Behavior

        protected void Awake()
        {
            m_PooledObjects = new List<GameObject>();
            if (m_Prefab)
            {
                for (int i = 0; i < m_PooledAmount; i++)
                {
                    GameObject newObject = Instantiate(m_Prefab);
                    newObject.SetActive(false);
                    m_PooledObjects.Add(newObject);
                }
            }
        }

        #endregion

        #region Unity_Behavior

        public GameObject GetPooledObject()
        {
            try
            {
                for (int i = 0; i < m_PooledObjects.Count; i++)
                {
                    if (!m_PooledObjects[i].activeInHierarchy)
                    {
                        return m_PooledObjects[i];
                    }
                }

                if (m_WillGrow)
                {
                    GameObject newObject = Instantiate(m_Prefab);
                    newObject.SetActive(false);
                    m_PooledObjects.Add(newObject);
                    return newObject;
                }
            }
            catch { }
            return null;
        }

        #endregion
    }
}