﻿using System;
using System.Numerics;
using Core.Enemies;
using Physics;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

namespace Core.Hero
{
    public class HeroBehavior : Unit
    {
        public const float standardAngle = 45f;

        #region Members

        public float rotationSpeed = 3f;

        public float throwStartSpeed = 7f;
        public float throwDistance = 5f;
        public Grenade grenadePrefab;
        public Transform throwPoint;

        protected bool isThrowing;
        protected Enemy nearestEnemy;

        [HideInInspector] public MyRigidbody rb;
        [HideInInspector] public Animator anim;

        #endregion

        #region Properties

        protected HeroAnimationState AnimState
        {
            get => (HeroAnimationState)anim.GetInteger("State");
            set => anim.SetInteger("State", (int)value);
        }

        #endregion

        #region Unity_Behavior

        private void Update()
        {
            SearchForTarget();
            AutoPilot();
            LookForward();
        }

        protected override void OnValidate()
        {
            base.OnValidate();
            rb = GetComponent<MyRigidbody>();
            anim = GetComponent<Animator>();
        }

        #endregion

        #region Protected_Methods

        protected void AutoPilot()
        {
            if (!nearestEnemy)
            {
                AnimState = HeroAnimationState.Idle;
                return;
            }
            if (Vector3.Distance(transform.position, nearestEnemy.transform.position) <= throwDistance)
            {
                Stop();
                Attack();
            }
            else
            {
                Move();
            }
        }

        protected void Attack()
        {
            isThrowing = true;
            AnimState = HeroAnimationState.Throw;
        }

        protected void LookForward()
        {
            if (isDead) return;
            if (!nearestEnemy) return;
            var direction = nearestEnemy.transform.position - transform.position;

            transform.rotation = Quaternion.Lerp(transform.rotation,
                Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
        }

        protected void SearchForTarget()
        {
            if (!nearestEnemy || nearestEnemy.isDead)
            {
                nearestEnemy = GameManager.instance.GetNearestEnemy(transform.position);
            }
        }

        protected void Stop()
        {
            var newVelocity = new Vector3(0, rb.velocity.y, 0);
            rb.velocity = newVelocity;
        }

        protected void Move()
        {
            if (isThrowing) return;
            AnimState = HeroAnimationState.Move;
            var direction = (nearestEnemy.transform.position - transform.position).normalized * speed;
            var newVelocity = new Vector3(direction.x, rb.velocity.y, direction.z);
            rb.velocity = newVelocity;
        }

        protected override void Death()
        {
            throw new System.NotImplementedException();
        }


        protected Vector3 DirectionOfLaunch(Vector3 targetPos, Vector3 launcherPos, float startSpeed, out bool inRange)
        {
            Vector3 targetDirection = targetPos - launcherPos;
            targetDirection.y = 0f;
            Quaternion targetDirRot = Quaternion.LookRotation(targetDirection);
            Vector3 targetLocalPos = Quaternion.Inverse(targetDirRot) * (targetPos - launcherPos);
            targetLocalPos.z = Mathf.Abs(targetLocalPos.z);

            float x = targetLocalPos.z;
            float y = targetLocalPos.y;

            float v = startSpeed;

            float g = Mathf.Abs(MyPhysics.Gravity.y);

            float ang;

            float root = Mathf.Sqrt(v * v * v * v - g * (g * (x * x) + 2 * y * (v * v)));
            if (root > 0)
            {
                float upP;
                upP = v * v + root;
                float dnP = g * x;
                float divRes = upP / dnP;
                float theta = Mathf.Atan(divRes);
                ang = theta * Mathf.Rad2Deg;
                inRange = true;
            }
            else
            {
                ang = standardAngle;
                inRange = false;
            }

            Vector3 launchDir = targetDirRot * Quaternion.Euler(-ang, 0, 0) * Vector3.forward;
            return launchDir;
        }

        #endregion


        #region Public_Methods

        //call from Animation Event
        public void Throw()
        {
            //nearestEnemy = GameManager.instance.GetNearestEnemy(transform.position);

            if(!nearestEnemy) return;
           
            bool isRange = false;
            Vector3 launchDir = DirectionOfLaunch(nearestEnemy.transform.position, throwPoint.position, throwStartSpeed, out isRange);

            var throwSpeed = throwStartSpeed;
            if (!isRange)
            {
                var L = Vector3.Distance(transform.position, nearestEnemy.transform.position);
                throwSpeed = Mathf.Sqrt((Mathf.Abs(MyPhysics.Gravity.y) * L) / (float)Math.Sin(2 * standardAngle * Mathf.Deg2Rad));
                launchDir = DirectionOfLaunch(nearestEnemy.transform.position, throwPoint.position, throwSpeed, out isRange);
            }

            Grenade newGrenade = Instantiate(grenadePrefab, throwPoint.position, Quaternion.identity);
            newGrenade.Launch(launchDir, throwSpeed) ;
        }

        //Animation Event
        public void CanMove()
        {
            isThrowing = false;
        }

        public override void ReceiveDamage(float damage)
        {
            throw new System.NotImplementedException();
        }

        #endregion

        #region Public_Methods



        #endregion
    }
}

public enum HeroAnimationState
{
    Idle,
    Move,
    Throw
}