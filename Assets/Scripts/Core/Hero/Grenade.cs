﻿using System.Security.Cryptography;
using Physics;
using UnityEngine;

namespace Core.Hero
{
    public class Grenade : MonoBehaviour
    {
        public float damage = 2f;
        public float radius = 5f;
        public GameObject explosionParticles;
        public int maxCollisionCount = 3;

        [HideInInspector]
        public MyBoxCollider boxCollider;

        [HideInInspector] public MyRigidbody rb;

        protected int collisionCount = 0;

        protected Vector3 currentDirection;
        protected float currentSpeed;

        protected void OnEnable()
        {
            //Destroy(gameObject, 15f);
        }

        protected void OnValidate()
        {
            boxCollider = GetComponent<MyBoxCollider>();
            rb = GetComponent<MyRigidbody>();
        }

        protected void Awake()
        {
            boxCollider.OnCollisionEnter += CollisionEnter;
        }

        protected void CollisionEnter(MyCollider other)
        {
            Explosion(damage);
        }

        protected void Explosion(float dam)
        {
            collisionCount++;
            if (collisionCount >= maxCollisionCount)
            {
                Destroy(gameObject);
            }

            if (explosionParticles)
            {
                GameObject explosion = Instantiate(explosionParticles, transform.position, Quaternion.identity);
                Destroy(explosion, 2f);
            }
            var enemies = GameManager.instance.GetEnemiesInRadius(transform.position, radius);

            foreach (var enemy in enemies)
            {
                enemy.ReceiveDamage(dam);

            }
        }

        public void Launch(Vector3 direction, float speed)
        {
            currentDirection = direction;
            currentSpeed = speed;

            rb.AddForce(direction * speed);
        }
    }
}
