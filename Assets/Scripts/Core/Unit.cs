﻿using Events;
using UnityEngine;

namespace Core
{
    public abstract class Unit : MonoBehaviour
    {
        public float health = 2f;
        public float speed = 4f;

        public GameEvent onEnable;
        public GameEvent onDeath;

        [HideInInspector] public new Transform transform;

        [HideInInspector]
        public bool isDead = false;

        public abstract void ReceiveDamage(float damage);

        protected abstract void Death();

        protected virtual void OnValidate()
        {
            transform = GetComponent<Transform>();
        }
    }
}
