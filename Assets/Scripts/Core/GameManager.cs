﻿using System.Collections.Generic;
using Core.Enemies;
using UnityEngine;

namespace Core
{
    public class GameManager : MonoBehaviour
    {
        #region Members

        public static GameManager instance;

        protected List<Enemy> enemies = new List<Enemy>();

        #endregion

        #region Unity_Behavior

        protected void Awake()
        {
            instance = this;
        }

        #endregion

        #region Public_Methods

        public void AddEnemy(MonoBehaviour author)
        {
            if (author is Enemy enemy)
            {
                enemies.Add(enemy);
            }
            
        }

        public void RemoveEnemy(MonoBehaviour author)
        {
            if (author is Enemy enemy)
            {
                enemies.Remove(enemy);
            }
        }

        public Enemy[] GetEnemiesInRadius(Vector3 position, float radius)
        {
            List<Enemy> overlappedEnemies = new List<Enemy>();

            foreach (var enemy in enemies)
            {
                if (Vector3.Distance(enemy.transform.position, position) < radius)
                {
                    overlappedEnemies.Add(enemy);
                }
            }

            return overlappedEnemies.ToArray();
        }

        public Enemy GetNearestEnemy(Vector3 position)
        {
            if (enemies.Count == 0) return null;
            Enemy nearestEnemy = enemies[0];

            for (int i = 1; i < enemies.Count; i++)
            {
                if (!enemies[i].isDead && Vector3.Distance(enemies[i].transform.position, position) <
                    Vector3.Distance(nearestEnemy.transform.position, position))
                {
                    nearestEnemy = enemies[i];
                }
            }

            return nearestEnemy;
        }

        #endregion

    }
}
