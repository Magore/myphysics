﻿using System.Data;
using UnityEngine;

namespace Core.Enemies
{
    public class Enemy : Unit
    {
        public const float distanceToWayPoint = 0.1f;

        #region Members

        public float rotationSpeed = 30f;

        public bool isMoving = true;

        public Transform[] wayPoints;

        [HideInInspector]
        public Animator anim;


        protected int currentWayPoint = 0;

        #endregion

        #region Properties

        protected EnemyAnimationState AnimState
        {
            get => (EnemyAnimationState) anim.GetInteger("State");
            set => anim.SetInteger("State", (int) value);
        }

        #endregion

        #region Unity_Events

        private void OnEnable()
        {
            onEnable?.Invoke(this);
        }

        protected void Update()
        {
            Move();
            LookForward();
        }

        protected override void OnValidate()
        {
            base.OnValidate();
            anim = GetComponent<Animator>();
        }

        #endregion

        #region Protected_Methods

        protected void Move()
        {
            if(isDead) return;
            
            if (!isMoving || wayPoints.Length == 0)
            {
                AnimState = EnemyAnimationState.Idle;
                return;
            }

            AnimState = EnemyAnimationState.Walk;
            transform.position = Vector3.MoveTowards(transform.position, wayPoints[currentWayPoint].position, speed * Time.deltaTime);
            bool isClose =
                Vector3.Distance(new Vector3(transform.position.x, 0, transform.position.z), new Vector3(wayPoints[currentWayPoint].transform.position.x, 0, wayPoints[currentWayPoint].transform.position.z)) < distanceToWayPoint;
            if (isClose)
            {
                currentWayPoint = (currentWayPoint >= wayPoints.Length - 1) ? 0 : currentWayPoint + 1;
            }
        }

        protected void LookForward()
        {
            if (isDead) return;
            var direction = wayPoints[currentWayPoint].position - transform.position;

            transform.rotation = Quaternion.Lerp(transform.rotation,
                Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
        }

        #endregion

        #region Public_Methods

        public override void ReceiveDamage(float damage)
        {
            if (isDead) return;
            health -= damage;

            AnimState = EnemyAnimationState.ReceiveDamage;
            if (health <= 0)
            {
                Death();
            }
        }

        protected override void Death()
        {
            isDead = true;
            AnimState = EnemyAnimationState.Death;
            onDeath?.Invoke(this);
            Destroy(gameObject, 1.5f);
        }

        #endregion


    }

    public enum EnemyAnimationState
    {
        Idle,
        Walk,
        ReceiveDamage,
        Death
    }
}
